import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import dts from "vite-plugin-dts";

export default defineConfig({
  plugins: [
    vue(),
    dts({ cleanVueFileName: true, outputDir: "./dist/types", insertTypesEntry: true, copyDtsFiles: false }),
  ],
  build: {
    cssCodeSplit: true,
    target: "esnext",
    lib: {
      name: 'components',
      entry: "./src/components/all/index.ts",
      fileName: () => "index.js",
      formats: ["umd"]
    },
    rollupOptions: {
      external: [
        "vue",
        "sass-loader",
        "vue3-dropzone",
        "fslightbox-vue",
        "vue-tsc",
        "imask",
        "tom-select",
        "moment",
        "vue-tabler-icons",
        "@vitejs/plugin-vue",
        "typescript",
        "vite",
        "vite-plugin-dts"
      ],
      output: { globals: { vue: "Vue" } } },
  }
});
