import { createApp } from "vue";
import App from "./App.vue";

import "@tabler/core/dist/css/tabler.min.css";
import "@tabler/core/dist/css/tabler-vendors.min.css";
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

createApp(App).mount("#app");
