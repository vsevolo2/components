export interface FileResource {
    /**
     * Идентификатор файла
     * @type {number}
     * @memberof FileResource
     */
    id: number;
    /**
     * URL файла
     * @type {string}
     * @memberof FileResource
     */
    url: string;
    /**
     * Оригинальное название файла
     * @type {string}
     * @memberof FileResource
     */
    origin_name: string;
    /**
     * Размер файла в байтах
     * @type {number}
     * @memberof FileResource
     */
    size: number;
    /**
     * Дата и время создания файла
     * @type {string}
     * @memberof FileResource
     */
    created_at: string;
}

export interface FileSize {
    bytes: number,
    size: string,
    unit: string,
    human: string
}

export interface SelectItem {
    value: number | string,
    text: string,
}

export interface FileResourceEdit extends FileResource {
    fileId?: number
}
