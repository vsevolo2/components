export const blobToImage = (blob: Blob): HTMLImageElement => {
    const url = URL.createObjectURL(blob)
    let img = new Image()
    img.onload = () => {
        URL.revokeObjectURL(url)
    }
    img.src = url
    return img;
}

export const getMaxValue = (digits: number, precision: number): number => {
    let max = '';
    for (let i = 0; i < digits; i++)
        max += '9';
    for (let i = 0; i < precision; i++) {
        if (!i)
            max += ".";
        max += '9';
    }

    return parseFloat(max);
}

export const stringEndingByNum = (iNumber: number, endings: string[]): string => {
    iNumber = iNumber % 100;
    if (iNumber >= 11 && iNumber <= 19) {
        return endings[2];
    }
    let i = iNumber % 10;
    switch (i) {
        case 1:
            return endings[0];
        case 2:
        case 3:
        case 4:
            return endings[1];
        default:
            return endings[2]
    }
}

export const mainForm = {
    exportOptions: {
        _export: 1,
        fields: 1,
        exportDateTimeAuthor: 1,
        fileName: 'None',
        loaded: false,
    }
}