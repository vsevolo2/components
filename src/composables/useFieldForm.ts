import {computed, isRef, watch} from "vue";
// import {ValidationErrors} from "~/api";

/**
 * Миксин для полей ввода формы
 *
 * @param props
 * @param emit
 */
export default function useFieldForm(
    props: {
        name?: string;
        type?: string;
        modelValue?: string;
        errors?: {[key: string]: any | string}
    },
    emit: any
) {

    const model = computed({
        get () {
            return props.modelValue;
        },
        set (value) {
            return emit('update:modelValue', value)
        }
    });
    const name = computed(()=> props?.name as string)
    const name2 = computed(()=>name.value?.replaceAll('-','.'));


    const hasError = name ? computed((): boolean => !!props?.errors && (!!props?.errors[name.value] || !!props?.errors[name2.value])) : false;
    const error = computed((): string => {
        if (!name) return "";
        const errors = props?.errors;
        if (!errors) return "";
        const fieldError = errors[name.value]??errors[name2.value];
        if (!fieldError) return "";
        if (Array.isArray(errors[name.value]??errors[name2.value]))
            return fieldError[0];
        return fieldError as string;
    });

    watch(   // Убираем ошибку, если пользователь пытается её исправить
        () => props.modelValue,
        (newValue: any, oldValue: any) => {
            if (isRef(hasError) && hasError.value && newValue !== oldValue && props?.errors)
            {
                props.errors[name.value] = "";
            }
        }
    );

    return { model, hasError, error, name }
}

