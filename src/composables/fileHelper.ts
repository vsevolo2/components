import {FileResource, FileResourceEdit, FileSize} from "./types";

export const getHumanFileSize = (originalBytes?: string): FileSize => {
    const units = ['B','KB', 'MB','GB']

    const res = {
        bytes: 0,
        size: '0',
        unit: units[0],
        human: '0 ' + units[0],
    };

    const bytes = parseInt(originalBytes ?? '0')
    res.bytes = (isNaN(bytes) || bytes <= 0) ? 0 : bytes;

    let size = res.bytes

    for (let i = 0; i < units.length; i++) {
        res.size = String(Number(size.toFixed(2)));
        res.unit = units[i];

        if (size < 1000) {
            break;
        } else if (size <= 1024) {
            res.size = "1";
            res.unit = units[i + 1] ?? 'TB';
            break;
        }
        size /= 1024
    }

    res.human = res.size + ' ' + res.unit

    return res
}

export const showImage = ((file: File | FileResource, imgTypes = ['png', 'jpg', 'jpeg']) => {
    const name = "origin_name" in file ? file.origin_name : file.name;
    const mime = name?.split('.');
    return mime && mime.length > 1 && imgTypes.includes(mime[1].toLowerCase());
});

export const sliderImages = ((files: FileResource[]) => {
    if (files && files.length > 0) {
        return files.filter((file: FileResource) => showImage(file)).map((image: FileResource) => image.url)
    }
});

export const deleteFiles = (
    acceptFile: FileResourceEdit,
    fileList: Array<FileResourceEdit|Blob>,
    addFiles: Array<FileResourceEdit|Blob>,
    removeFiles: number[]
) => {
    if (acceptFile.id) {
        fileList = fileList?.filter((file: FileResourceEdit) => acceptFile.id !== file.id)
        removeFiles.push(acceptFile.id);
    } else {
        fileList = fileList?.filter((file: FileResourceEdit, index: number) => {
            return acceptFile.fileId !== index;
        });
        addFiles = addFiles?.filter((file: FileResourceEdit, index: number) => {
            return acceptFile.fileId !== index;
        });
    }
    return { fileList, addFiles, removeFiles };
};
