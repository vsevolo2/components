import {SelectItem} from "./types";
import moment from "moment/moment";

export const monthsFilter = (): SelectItem[] => {
    const thisMonth = moment().startOf('month').format('DD.MM.yyyy');
    const thisWeek = moment().startOf('w').format('DD.MM.yyyy');
    const yearAgo = moment().subtract(1,'year').format('DD.MM.yyyy');
    return [
        {
            value: thisWeek,
            text: 'Текущая неделя'
        },
        {
            value: thisMonth,
            text: 'Текущий месяц'
        },
        {
            value: yearAgo,
            text: '12 месяцев'
        }
    ];
};
