import { FileResource, FileResourceEdit, FileSize } from "./types";
export declare const getHumanFileSize: (originalBytes?: string) => FileSize;
export declare const showImage: (file: File | FileResource, imgTypes?: string[]) => boolean;
export declare const sliderImages: (files: FileResource[]) => string[];
export declare const deleteFiles: (acceptFile: FileResourceEdit, fileList: Array<FileResourceEdit | Blob>, addFiles: Array<FileResourceEdit | Blob>, removeFiles: number[]) => {
    fileList: (Blob | FileResourceEdit)[];
    addFiles: (Blob | FileResourceEdit)[];
    removeFiles: number[];
};
