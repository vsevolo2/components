/**
 * Миксин для полей ввода формы
 *
 * @param props
 * @param emit
 */
export default function useFieldForm(props: {
    name?: string;
    type?: string;
    modelValue?: string;
    errors?: {
        [key: string]: any | string;
    };
}, emit: any): {
    model: import("vue").WritableComputedRef<string>;
    hasError: boolean | import("vue").ComputedRef<boolean>;
    error: import("vue").ComputedRef<string>;
    name: import("vue").ComputedRef<string>;
};
