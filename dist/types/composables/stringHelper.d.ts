export declare const blobToImage: (blob: Blob) => HTMLImageElement;
export declare const getMaxValue: (digits: number, precision: number) => number;
