declare const _sfc_main: import("vue").DefineComponent<Readonly<{
    errors?: any;
    companyId?: any;
    form?: any;
}>, {
    props: any;
    emit: (event: "clear" | "setAddressData" | "getOptions", ...args: any[]) => void;
    selectedAddress: import("vue").Ref<string>;
    getAddress: import("vue").ComputedRef<string>;
    flatInfo: import("vue").ComputedRef<string>;
    formCompanyId: import("vue").ComputedRef<any>;
    refAuto: import("vue").Ref<any>;
    onBlur: () => void;
    setAutoComplete: () => Promise<void>;
}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("clear" | "setAddressData" | "getOptions")[], "clear" | "setAddressData" | "getOptions", import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<Readonly<{
    errors?: any;
    companyId?: any;
    form?: any;
}>>> & {
    onClear?: (...args: any[]) => any;
    onSetAddressData?: (...args: any[]) => any;
    onGetOptions?: (...args: any[]) => any;
}, {
    readonly errors?: any;
    readonly companyId?: any;
    readonly form?: any;
}, {}>;
export default _sfc_main;
