declare const _sfc_main: import("vue").DefineComponent<Readonly<{
    title?: any;
    id?: any;
    class?: any;
    text?: any;
    confirmButton?: any;
    confirmClass?: any;
    noAutoClose?: any;
    noHeader?: any;
    noFooter?: any;
    closed?: any;
    maxWidth?: any;
    confirmDisabled?: any;
}>, {
    props: any;
    emit: (event: "confirm" | "cancel", ...args: any[]) => void;
    confirmClasses: import("vue").ComputedRef<string>;
    show: import("vue").ComputedRef<boolean>;
    root: import("vue").Ref<any>;
    close: import("vue").Ref<any>;
    confirmEvent: () => void;
    cancelEvent: () => void;
}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("confirm" | "cancel")[], "confirm" | "cancel", import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<Readonly<{
    title?: any;
    id?: any;
    class?: any;
    text?: any;
    confirmButton?: any;
    confirmClass?: any;
    noAutoClose?: any;
    noHeader?: any;
    noFooter?: any;
    closed?: any;
    maxWidth?: any;
    confirmDisabled?: any;
}>>> & {
    onConfirm?: (...args: any[]) => any;
    onCancel?: (...args: any[]) => any;
}, {
    readonly title?: any;
    readonly id?: any;
    readonly class?: any;
    readonly text?: any;
    readonly confirmButton?: any;
    readonly confirmClass?: any;
    readonly noAutoClose?: any;
    readonly noHeader?: any;
    readonly noFooter?: any;
    readonly closed?: any;
    readonly maxWidth?: any;
    readonly confirmDisabled?: any;
}, {}>;
export default _sfc_main;
