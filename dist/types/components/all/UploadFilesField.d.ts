import { FileSize } from "../../composables/types";
declare const _sfc_main: import("vue").DefineComponent<Readonly<{
    name?: any;
    errors?: any;
    disabled?: any;
    files?: any;
    format?: any;
    max_value?: any;
    isMini?: any;
    imageSize?: any;
    imagesInline?: any;
}>, {
    props: any;
    emit: (event: "delete" | "onDrop" | "sliderOpen", ...args: any[]) => void;
    hasError: boolean | import("vue").ComputedRef<boolean>;
    error: import("vue").ComputedRef<string>;
    imageSize: import("vue").ComputedRef<any>;
    openView: (link: string) => void;
    number: import("vue").ComputedRef<number>;
    countFiles: import("vue").ComputedRef<any>;
    imagesInLine: import("vue").ComputedRef<any>;
    countStr: import("vue").ComputedRef<string>;
    minHeight: import("vue").ComputedRef<string>;
    onDrop: (acceptFiles: File[]) => void;
    getFileUrl: (file: any) => string;
    deleteFile: (file: any, index: number) => boolean;
    getRootProps: ({ onFocus, onBlur, onClick, onDragEnter, onDragenter, onDragOver, onDragover, onDragLeave, onDragleave, onDrop, ...rest }?: {
        [key: string]: any;
    }) => {
        tabIndex?: number;
        onFocus: () => void;
        onBlur: () => void;
        onClick: () => void;
        onDragenter: () => void;
        onDragover: () => void;
        onDragleave: () => void;
        onDrop: () => void;
        ref: import("vue").Ref<import("vue").RendererElement>;
    };
    getInputProps: ({ onChange, onClick, ...rest }?: {
        onChange?: () => void;
        onClick?: () => void;
    }) => {
        accept: string;
        multiple: boolean;
        style: string;
        type: string;
        onChange: () => void;
        onClick: () => void;
        autoComplete: string;
        tabIndex: number;
        ref: import("vue").Ref<import("vue").RendererElement>;
    };
    open: () => void;
    maxFileSize: import("vue").ComputedRef<FileSize>;
    fileSize: (file?: {
        size?: number;
    }) => FileSize;
    fileSizeShortener: (size?: FileSize) => FileSize;
    readonly showImage: (file: File | import("../../composables/types").FileResource, imgTypes?: string[]) => boolean;
    readonly TrashIcon: import("vue-tabler-icons").TablerIconComponent;
}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("delete" | "onDrop" | "sliderOpen")[], "delete" | "onDrop" | "sliderOpen", import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<Readonly<{
    name?: any;
    errors?: any;
    disabled?: any;
    files?: any;
    format?: any;
    max_value?: any;
    isMini?: any;
    imageSize?: any;
    imagesInline?: any;
}>>> & {
    onDelete?: (...args: any[]) => any;
    onOnDrop?: (...args: any[]) => any;
    onSliderOpen?: (...args: any[]) => any;
}, {
    readonly name?: any;
    readonly errors?: any;
    readonly disabled?: any;
    readonly files?: any;
    readonly format?: any;
    readonly max_value?: any;
    readonly isMini?: any;
    readonly imageSize?: any;
    readonly imagesInline?: any;
}, {}>;
export default _sfc_main;
