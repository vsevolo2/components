declare const _sfc_main: import("vue").DefineComponent<Readonly<{
    name?: any;
    errors?: any;
    image?: any;
}>, {
    uploadImage: import("vue").Ref<{
        valueOf: () => boolean;
    }>;
    props: any;
    image: import("vue").Ref<HTMLInputElement>;
    emit: (event: "delete" | "upload", ...args: any[]) => void;
    hasError: any;
    error: any;
    openUploadFile: () => void;
    changeUploadImage: (e: Event) => void;
    deleteImage: () => void;
    readonly UserIcon: import("vue-tabler-icons").TablerIconComponent;
    readonly TrashIcon: import("vue-tabler-icons").TablerIconComponent;
}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, ("delete" | "upload")[], "delete" | "upload", import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<Readonly<{
    name?: any;
    errors?: any;
    image?: any;
}>>> & {
    onDelete?: (...args: any[]) => any;
    onUpload?: (...args: any[]) => any;
}, {
    readonly name?: any;
    readonly errors?: any;
    readonly image?: any;
}, {}>;
export default _sfc_main;
