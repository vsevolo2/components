import TomSelect from "tom-select";
declare const _sfc_main: import("vue").DefineComponent<Readonly<{
    id?: any;
    name?: any;
    modelValue?: any;
    errors?: any;
    disabled?: any;
    placeholder?: any;
    canAdd?: any;
    items?: any;
    multiple?: any;
    noInput?: any;
    map_items?: any;
}>, {
    props: any;
    emit: (event: "update:modelValue", ...args: any[]) => void;
    model: import("vue").WritableComputedRef<string>;
    hasError: boolean | import("vue").ComputedRef<boolean>;
    error: import("vue").ComputedRef<string>;
    refSelect: import("vue").Ref<any>;
    idModel: import("vue").ComputedRef<any>;
    items: import("vue").Ref<any>;
    updateOptions: () => void;
    removeOption: (value: string) => void;
    clearField: (selectField: TomSelect) => void;
    root: import("vue").Ref<any>;
}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "update:modelValue"[], "update:modelValue", import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<Readonly<{
    id?: any;
    name?: any;
    modelValue?: any;
    errors?: any;
    disabled?: any;
    placeholder?: any;
    canAdd?: any;
    items?: any;
    multiple?: any;
    noInput?: any;
    map_items?: any;
}>>> & {
    "onUpdate:modelValue"?: (...args: any[]) => any;
}, {
    readonly id?: any;
    readonly name?: any;
    readonly modelValue?: any;
    readonly errors?: any;
    readonly disabled?: any;
    readonly placeholder?: any;
    readonly canAdd?: any;
    readonly items?: any;
    readonly multiple?: any;
    readonly noInput?: any;
    readonly map_items?: any;
}, {}>;
export default _sfc_main;
